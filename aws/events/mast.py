import boto3
import json
import os
import time
import pymsteams
import logging
import requests
import json

from elasticsearch import Elasticsearch, helpers, RequestsHttpConnection
from requests_aws4auth import AWS4Auth

logger = logging.getLogger()
logger.setLevel(logging.INFO)

teams_webhook_url = os.environ['teams_channel_webhook']. # https://docs.microsoft.com/en-us/microsoftteams/platform/webhooks-and-connectors/how-to/add-incoming-webhook

domain = os.environ['es_url']
# indexSizeLimit = os.environ['indexSizeLimit']
# shardsUtlPecentLimit = os.environ['shardsUtlPecentLimit']

session = boto3.session.Session()
credentials = session.get_credentials()
awsauth = AWS4Auth(
    credentials.access_key,
    credentials.secret_key,
    session.region_name,
    'es',
    session_token=credentials.token)
es = Elasticsearch(
    [os.environ['es_url']],
    scheme='https',
    port=443,
    http_auth=awsauth,
    use_ssl=True,
    verify_certs=True,
    connection_class=RequestsHttpConnection)


def lambda_handler(event, context):
    try:
        indexFindings = calIndexSize()
        if indexFindings != {}:
            print("Trying to send teams notification on index")
            notify_teams(indexFindings)
    except Exception as e:
        raise
        _erroNotifyTeams(e, "Error while auditing Index size", context)
    try:
        shardsFindings = calShardsSize()
        if shardsFindings != {}:
            print("Trying to send teams notification on shards")
            notifyTeamsShards(shardsFindings)
    except Exception as e:
        raise
        _erroNotifyTeams(
            e, "Error while auditing Shards usage monitor", context)


def calIndexSize():
    indices = es.cat.indices(format="json")
    resources = {}
    for index in indices:
        if not index["index"].startswith('.'):
            if 'gb' in index['store.size'] and '000001' in index["index"]:
                size = int(float(index['store.size'][:-2]))
                if size > 45:
                    resources[index['index']] = {}
                    resources[index['index']]["Size"] = index['store.size']
                    resources[index['index']]["PriShards"] = index['pri']
                    get_index_info_details = explain_index_ism_policy(
                        index["index"])
                    get_index_info = get_index_info_details[index['index']]
                    resources[index['index']
                              ]["AttachedISMPolicy"] = str(get_index_info['index.opendistro.index_state_management.policy_id'])
                    try:
                        if get_index_info['action']['failed'] == True:
                            resources[index['index']
                                      ]["IsmActionStatus"] = "Failed"
                        elif get_index_info['retry_info']['failed'] == True:
                            resources[index['index']
                                      ]["IsmActionStatus"] = "RetryFailed"
                        else:
                            resources[index['index']
                                      ]["IsmActionStatus"] = "Success"
                    except:
                        resources[index['index']
                                  ]["IsmActionStatus"] = "NA"
                    try:
                        resources[index['index']
                                  ]["IsmMessage"] = get_index_info['info']['message']
                    except:
                        resources[index['index']
                                  ]["IsmMessage"] = "NA"
                    resources[index['index']]["Health"] = index['health']
    return resources


def explain_index_ism_policy(index):
    path = "/_opendistro/_ism/explain/" + index
    url = os.environ['es_url'] + path
    explanation = requests.get(url, auth=awsauth)
    explanation_data = explanation.json()
    state = {}
    for key in explanation_data:
        state[key] = explanation_data[key]
    return state


def notify_teams(error_msg):
    r = json.dumps(error_msg)
    resources = json.loads(r)
    try:
        cclTeamErrorMsg = pymsteams.connectorcard(teams_webhook_url)
        cclTeamErrorMsg.title("Detected Over Sized >45GB Indices")
        cclTeamErrorMsg.text(
            "30Gb is the limit to rollover an index to warm state and it directly affects the stability and performance of your CCL Elasticsearch cluster")
        for key in resources:
            value = resources[key]
            index = key
            # Create a  message section for each key
            print(key, value)
            key = pymsteams.cardsection()
            key.activityTitle(index)
            key.addFact("Size:", value['Size'])
            key.addFact("PrimaryShards:", value['PriShards'])
            key.addFact("AttachedISMPolicy:", value['AttachedISMPolicy'])
            key.addFact("ISMActionStatus:", value['IsmActionStatus'])
            key.addFact("IsmMessage:", value['IsmMessage'])
            key.addFact("Health:", value['Health'])
            cclTeamErrorMsg.addSection(key)
        cclTeamErrorMsg.send()
    except Exception as e:
        print("Not able to send teams notification on Index size monitor")
        logger.info(e)
        raise


def notifyTeamsShards(error_msg):
    r = json.dumps(error_msg)
    resourceShards = json.loads(r)
    try:
        cclTeamErrorMsg = pymsteams.connectorcard(teams_webhook_url)
        cclTeamErrorMsg.title("Detected Over Usage > 60% of Cluster Shards")
        cclTeamErrorMsg.text(
            "There is a Total Shards limit on cluster as per settings, over usage of shards can directly affects the stability and performance of your CCL Elasticsearch cluster")
        key = pymsteams.cardsection()
        key.addFact("ClusterNodes:", resourceShards['ClusterNodes'])
        key.addFact("ClusterFailedNodes:",
                    resourceShards['ClusterFailedNodes'])
        key.addFact("ClusterSuccessfulNodes:",
                    resourceShards['ClusterSuccessfulNodes'])
        key.addFact("ClusterMaxShardsPerNode:",
                    resourceShards['ClusterMaxShardsPerNode'])
        key.addFact("TotalShardsCount:", resourceShards['TotalShardsCount'])
        key.addFact("AvgActualShardsPerNode:",
                    resourceShards['AvgActualShardsPerNode'])
        key.addFact("ClusterTotalShardsCapacity:",
                    resourceShards['ClusterTotalShardsCapacity'])
        key.addFact("ClusterUtilizationPercentage:",
                    resourceShards['ClusterUtilizationPercentage'])
        cclTeamErrorMsg.addSection(key)
        cclTeamErrorMsg.send()
    except Exception as e:
        print("Not able to send teams notification on shards usage monitor")
        logger.info(e)
        raise


def _erroNotifyTeams(error, title, context):
    ErrorcclTeamErrorMsg = pymsteams.connectorcard(teams_webhook_url)
    ErrorcclTeamErrorMsg.title(title)
    ErrorcclTeamErrorMsg.summary(str(context.function_name))
    ErrorcclTeamErrorMsg.text(str(error))
    ErrorcclTeamErrorMsg.send()


def calShardsSize():
    """
    Information to extract from cluster
        :ClusterNodes
        :ClusterFailedNodes
        :ClusterSuccessfulNodes
        :ClusterMaxShardsPerNode
        :ActualShardsPerNode
        :TotalShardsCount
        :AvgActualShardsPerNode
        :ClusterTotalShardsCapacity
        :ClusterUtilizationPercentage
    """
    audit = {}
    audit["ClusterNodes"] = get_total_nodes()["get_nodes_count"]
    audit["ClusterFailedNodes"] = get_total_nodes()["failed_get_nodes_count"]
    audit["ClusterSuccessfulNodes"] = get_total_nodes()[
        "success_get_nodes_count"]
    audit["ClusterMaxShardsPerNode"] = get_cluster_nodes_from_settings()
    audit["TotalShardsCount"] = get_total_shards()
    audit["AvgActualShardsPerNode"] = (int(audit["ClusterSuccessfulNodes"]) *
                                       int(audit["ClusterMaxShardsPerNode"])) / int(audit["TotalShardsCount"])
    audit["ClusterTotalShardsCapacity"] = int(
        audit["ClusterSuccessfulNodes"]) * int(audit["ClusterMaxShardsPerNode"])
    audit["ClusterUtilizationPercentage"] = 100*(int(
        audit["TotalShardsCount"])/int(audit["ClusterTotalShardsCapacity"]))
    if audit["ClusterUtilizationPercentage"] > 50:
        return audit
    else:
        return audit


def get_cluster_nodes_from_settings():
    path = "/_cluster/settings/"
    url = domain + path
    explanation = requests.get(url, auth=awsauth)
    try:
        max_nodes_per_cluster = explanation.json(
        )["persistent"]["cluster"]["max_shards_per_node"]
    except KeyError:
        max_nodes_per_cluster = '1000'
    return max_nodes_per_cluster


def get_total_nodes():
    path = "/_nodes/stats?format=json"
    url = domain + path
    get_nodes = requests.get(url, auth=awsauth)
    get_nodes_count = get_nodes.json()["_nodes"]["total"]
    failed_get_nodes_count = get_nodes.json()["_nodes"]["failed"]
    success_get_nodes_count = get_nodes.json()["_nodes"]["successful"]
    return {"get_nodes_count": get_nodes_count, "success_get_nodes_count": success_get_nodes_count, "failed_get_nodes_count": failed_get_nodes_count}


def get_total_shards():
    total_shards = 0
    path = '/_cat/allocation?format=json'
    url = domain + path
    get_node_info = requests.get(url, auth=awsauth)
    nodes_info_response = get_node_info.json()
    for each in nodes_info_response:
        total_shards = total_shards + int(each["shards"])
    return total_shards

